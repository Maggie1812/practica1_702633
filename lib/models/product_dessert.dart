
import 'dart:math';
import 'package:flutter/foundation.dart';

enum OrderSize { CH, M, G }

class ProductDessert {
  final String productTitle; // nombre del producto
  final String productDescription; // descripcion del producto
  final String productImage; // url de imagen del producto
  OrderSize orderSize; // tamano del producto
  double productPrice; // precio del producto autocalculado
  int productAmount; // cantidad de producto por comprar
  bool liked;

  ProductDessert({
    @required this.productTitle,
    @required this.productDescription,
    @required this.productImage,
    @required this.orderSize,
    @required this.productAmount,
    this.liked = false,
  }) {
    // inicializa el precio de acuerdo a la size del producto
    productPrice = productPriceCalculator();
  }

  // Mandar llamar este metodo cuando se cambie el tamanio del producto
  // De esta manera el precio del nuevo tamanio del producto se autocalcula
  // Por ejemplo cuando se cambie a M
  //
  // FlatButton(
  //   child: Text("M"),
  //   onPressed: () {
  //     setState(() {
  //       prod.productSize = ProductSize.M;
  //       prod.productPrice = prods.productPriceCalculator();
  //     });
  //   },
  // ),
  //
  //
  double productPriceCalculator() {
    if (this.orderSize == OrderSize.CH)
      return (20 + Random().nextInt(40)).toDouble();
    if (this.orderSize == OrderSize.M)
      return (40 + Random().nextInt(60)).toDouble();
    if (this.orderSize == OrderSize.G)
      return (60 + Random().nextInt(80)).toDouble();
    return 999.0;
  }
}


// TODO: Agregar al ProductRepository una lista de estos productos.
