import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/models/product_hot_drinks.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/cart/cart.dart';


//enum ProductSize { CH, M, G }
 
class ItemHotDrinksDetails extends StatefulWidget {
  ProductHotDrinks drink;
  List<ProductItemCart> productList;
  ItemHotDrinksDetails({
    Key key,
    @required this.drink,
    @required this.productList,
  }) : super(key: key);

  @override
  _HotDrinkDetailState createState() => _HotDrinkDetailState();
  }
  

class _HotDrinkDetailState extends State<ItemHotDrinksDetails>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var _selectedIndex = ProductSize.M;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey, 
      appBar: AppBar(
        title: Text(widget.drink.productTitle),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Cart(
                  productsList: widget.productList)
                  ),
               );
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 48, horizontal: 24),
                height: MediaQuery.of(context).size.height / 3,
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Image.network(
                          widget.drink.productImage, 
                          height: 160,
                          width: 160
                        )
                      )
                    ),
                   // Container(
                     // color: Colors.orange,
                   // ),
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        icon: Icon(Icons.favorite,
                          color: widget.drink.liked? Colors.redAccent : PRIMARY_COLOR),
                          onPressed: () {
                            setState(() {
                              widget.drink.liked = !widget.drink.liked;
                            });
                        },                     ),
                    ),

                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 28),
                child: Text(widget.drink.productTitle),
              ),
              Text(widget.drink.productDescription),
              SizedBox(
                height: 48,
              ),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Text("TAMAÑOS DISPONIBLES"),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Text("Precio"),
                        Text("\$ " + widget.drink.productPrice.toString()),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ChoiceChip(
                    label: Text('Chico'),
                    labelStyle: TextStyle(
                      color: _selectedIndex == ProductSize.CH ? Colors.white : Colors.blue,
                    ),
                    selected: _selectedIndex == ProductSize.CH,
                    backgroundColor: Colors.white,
                    selectedColor: Colors.blue,
                    onSelected: (bool selected) {
                      setState(() {
                        if (selected){
                          _selectedIndex = ProductSize.CH;
                          widget.drink.productSize = ProductSize.CH;
                          widget.drink.productPrice = widget.drink.productPriceCalculator();
                        }                       
                      });
                    },
                  ),
                  ChoiceChip(
                    label: Text('Mediano'),
                    selected: _selectedIndex == ProductSize.M,
                    labelStyle: TextStyle(
                      color: _selectedIndex == ProductSize.M ? Colors.white : Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    selectedColor: Colors.blue,
                    onSelected: (bool selected) {
                      setState(() {
                        if (selected) {
                          _selectedIndex = ProductSize.M;
                          widget.drink.productSize = ProductSize.M;
                          widget.drink.productPrice = widget.drink.productPriceCalculator();

                        }
                      });
                    },
                  ),
                  ChoiceChip(
                    label: Text('Grande'),
                    selected: _selectedIndex == ProductSize.G,
                    labelStyle: TextStyle(
                      color: _selectedIndex == ProductSize.G ? Colors.white : Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    selectedColor: Colors.blue,
                    onSelected: (bool selected) {
                      setState(() {
                        if (selected){
                          _selectedIndex = ProductSize.G;
                        widget.drink.productSize = ProductSize.G;
                        widget.drink.productPrice = widget.drink.productPriceCalculator();
                        }
                      });
                    },
                  ),
                ]
              ),
              Row(
                children: [SizedBox(
                  height: 80,
                )]
              ),
              Row(
                children: [
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {
                        widget.drink.productAmount += 1;
                        widget.productList.retainWhere((element) => element.productTitle != widget.drink.productTitle);
                        ProductItemCart _productItemCart = ProductItemCart(
                          productAmount: widget.drink.productAmount,
                          productTitle: widget.drink.productTitle,
                          productPrice: widget.drink.productPrice,
                          productImage: widget.drink.productImage,
                          productDescription: widget.drink.productDescription,
                          productSize: widget.drink.productSize.toString());
                          widget.productList.add(_productItemCart);
                      },
                      child: Text("AGREGAR AL CARRITO",
                      textAlign: TextAlign.center),
                      color: Colors.grey[300],
                      height: 60,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {},
                      child: Text("COMPRAR AHORA",
                      textAlign: TextAlign.center),
                      color: Colors.grey[300],
                      height: 60,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );

  }
  
}