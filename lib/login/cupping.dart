import 'package:flutter/material.dart';
import 'package:estructura_practica_1/login/login.dart';



class Cupping extends StatefulWidget {
  Cupping({Key key}) : super(key: key);
  @override
  _CuppingState createState() => _CuppingState();
}

class _CuppingState extends State<Cupping> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
    
      body: Stack(
        children: <Widget>[
          Container(
            child: GestureDetector(onTap: _launchLogin),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage('https://images.unsplash.com/photo-1589830802010-c2c25d784842?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'),
                fit: BoxFit.cover),
            )
          ),
          
          Container(
            child: Align(
              alignment: Alignment.center,
              //onTap: _launchLogin,
              child: Image.asset("lib/media/cupping.png",
              width: MediaQuery.of(context).size.width),
            ),
          )
        ],
      ),
    );
  }
              
  void _launchLogin() {
   Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => Login()),
    );
  }
}
