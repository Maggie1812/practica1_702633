import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:estructura_practica_1/home/home.dart';

class Access extends StatefulWidget{
    Access({Key key,}) : super(key: key);

  @override
 _AccessState createState() => _AccessState();

 
}

class _AccessState extends State<Access> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Container(
            color: PRIMARY_COLOR,
          ),
          Column(
            children: <Widget>[
              
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                height: MediaQuery.of(context).size.height * 0.30,
                width: MediaQuery.of(context).size.width * 0.60,
                child: Image.asset("lib/media/cupping.png", height: MediaQuery.of(context).size.height * 0.50,width: MediaQuery.of(context).size.width * 0.60,),
                ),
              ),
              SizedBox(height: 20,),
              Align(
                alignment:  Alignment.centerLeft,
                child: Text("       Nombre completo", 
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                  )
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                child: TextField(
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    border: OutlineInputBorder(),
                    hintText: "Anna Smith"
                  ),
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
              ),)
              ,
              SizedBox(height: 30,),
              Align(
                alignment:  Alignment.centerLeft,
                child: Text("       Password", 
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                  )
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                color: PRIMARY_COLOR,
                child: TextField(
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white)
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide:BorderSide(color: Colors.white)
                    ),
                  ),
                  obscureText: true,
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
                ),
              ),
              SizedBox(height: 30),
              SizedBox(height: 20,),
              Container(
                child: FlatButton(
                  color: BUTTON_COLOR,
                  child: Text("ENTRAR"),
                  onPressed: _home
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
              ),
             
            ],
          )
        ],
      ),
    );
  }

  void _home(){
     Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => Home(title: APP_TITLE)),
    );
  }
}