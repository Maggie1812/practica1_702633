import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:estructura_practica_1/login/register.dart';
import 'package:estructura_practica_1/login/access.dart';


class Login extends StatefulWidget {
  Login({Key key,}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  void initState() {
    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    
      body: Stack(
        children: <Widget>[
          Container(
            color: PRIMARY_COLOR,
          ),
          Column(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                height: MediaQuery.of(context).size.height * 0.40,
                width: MediaQuery.of(context).size.width * 0.6,
                child: Image.asset("lib/media/cupping.png"),
                ),
              ),
              SizedBox(height: 50,),
              Align(
                alignment: Alignment.center,
                child: Container(
                color: BUTTON_COLOR,
                child: FlatButton(
                  child: Text("REGISTRATE"),
                  onPressed: _gotoRegister,
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
              ),)
              ,
              SizedBox(height: 50,),
              Container(
                child: FlatButton(
                  color: BUTTON_COLOR,
                  child: Text("INGRESA"),
                  onPressed: _gotoAccess,
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
              ),
              
            ],
          )
        ],
      ),
    );
  }
              
  void _gotoRegister() {
       Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => Register()),
    );
  }
void _gotoAccess() {
       Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => Access()),
    );
  }
}
    