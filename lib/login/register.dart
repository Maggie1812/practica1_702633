import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:estructura_practica_1/login/access.dart';

class Register extends StatefulWidget {
  Register({Key key,}) : super(key: key);
  bool _TermsAndConditions = false;

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Stack(
        children: <Widget>[
          Container(
            color: PRIMARY_COLOR,
          ),
          Column(
            children: <Widget>[
              
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                height: MediaQuery.of(context).size.height * 0.30,
                width: MediaQuery.of(context).size.width * 0.60,
                child: Image.asset("lib/media/cupping.png", height: MediaQuery.of(context).size.height * 0.50,width: MediaQuery.of(context).size.width * 0.60,),
                ),
              ),
              SizedBox(height: 20,),
              Align(
                alignment:  Alignment.centerLeft,
                child: Text("       Nombre completo", 
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                  )
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                color: Colors.grey[400],
                child: TextField(
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    border: OutlineInputBorder(),
                    hintText: "Anna Smith"
                  ),
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
              ),)
              ,
              SizedBox(height: 30,),
              Align(
                alignment:  Alignment.centerLeft,
                
                child: Text("       Email", 
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                  )
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                color: Colors.grey[400],
                child: TextField(
                  decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    border: OutlineInputBorder(),
                    hintText: "annasmith@xpert.com"
                  ),
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
                ),
              ),
              SizedBox(height: 30,),
              Align(
                alignment:  Alignment.centerLeft,
                child: Text("       Password", 
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white,
                  )
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                color: PRIMARY_COLOR,
                child: TextField(
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white)
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide:BorderSide(color: Colors.white)
                    ),
                  ),
                  obscureText: true,
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
                ),
              ),
              SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Checkbox(
                    value: widget._TermsAndConditions, 
                    checkColor: Colors.white,
                    onChanged: (bool value) { 
                      widget._TermsAndConditions = !widget._TermsAndConditions;
                      setState(() {});
                     }),
                  Text("ACEPTO LOS TEMRINOS Y CONDICIONES DE USO",
                  style: TextStyle(color: Colors.white),)
                ],
              ),
              SizedBox(height: 20,),
              Container(
                child: FlatButton(
                  color: BUTTON_COLOR,
                  child: Text("REGISTATE"),
                  onPressed: _register
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
              ),
              SizedBox(height: 20,),
              Text("¿Ya tienes una cuenta?",
                style: TextStyle(
                  color: Colors.white
                ),
              ),
              FlatButton(onPressed: _access,
               child: Text("INGRESA",
                style: TextStyle(color: Colors.white,),
                ),
              )
              
              
            ],
          )
        ],
      ),
    );
  }

  void _register(){
showDialog(
     context: context,
     builder: (BuildContext context){
       return AlertDialog(
         title: Text('REGISTRO'),
         content: Text("Registro realizado con exito"),
         actions: <Widget>[
           new FlatButton(
             onPressed: () {
               Navigator.of(context).pop();
             }, 
           child: Text("Aceptar",
            style: TextStyle(
              color: PRIMARY_COLOR),)
           )
         ]);
     }
   );
   }

  void _access(){
     Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => Access()),
    );
  }
}