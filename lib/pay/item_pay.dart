import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

class ItemPay extends StatelessWidget {
  final String title;
  final Widget image;
  ItemPay({
    Key key,
    @required this.title,
    @required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      child: Stack(
        children: [
          Container(
            height: 160,
            margin: EdgeInsets.only(left: 24, right: 24),
            decoration: BoxDecoration(
              color: BACKGROUND_COLOR,
              borderRadius: BorderRadius.all(
                Radius.circular(2),
              ),
            ),
            child: Row(
              children: [
                Align(
            alignment: Alignment.centerRight,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(5.0),
                bottomRight: Radius.circular(5.0),
              ),
              child: image
            ),
          ),
                Container(
                  margin: EdgeInsets.only(left: 16),
                  width: 160,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(30),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Text(
                      "$title",
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(fontWeight: FontWeight.normal,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 30,
            right: 30,
            child: Icon(Icons.mode_edit, color: PRIMARY_COLOR,),)
          
        ],
      ),
    );
  }
}
