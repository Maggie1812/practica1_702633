import 'package:estructura_practica_1/desserts/desserts_page.dart';
import 'package:estructura_practica_1/drinks/hot_drinks_page.dart';
import 'package:estructura_practica_1/grains/grains_page.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/pay/item_pay.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:estructura_practica_1/models/product_repository.dart';
import 'package:estructura_practica_1/cart/cart.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';



class Pay extends StatefulWidget {
  List<ProductItemCart> _productsList = [];

  Pay({Key key}) : super(key: key);

  @override
  _PayState createState() => _PayState();
}

class _PayState extends State<Pay>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Pagos"),
        actions: <Widget>[
         
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Cart(
                  productsList: widget._productsList)),
               );
            },
          )
        ],
      ),
      
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width *0.06, top: 15),
            height: 40,
            child: Text("Elige un método de pago:",
            style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(fontWeight: FontWeight.normal,
                            fontSize: 22),
            ),
          ),
          GestureDetector(
            onTap: _showPayment,
            child: ItemPay(
              title: "Tarjeta de Credito",
              image: Container(
                width: MediaQuery.of(context).size.width *0.4,
                child: Icon(Icons.credit_card,
                    color: PRIMARY_COLOR,
                    size: 60, ),)
            ),
          ),
          GestureDetector(
            onTap: _showPayment,
            child: ItemPay(
              title: "PayPal",
              image: Image.asset("lib/media/paypal.png",
              width: MediaQuery.of(context).size.width *0.4)),
          ),
          GestureDetector(
            onTap: _showPayment,
            child: ItemPay(
              title: "Postres",
              image: Container(
                width: MediaQuery.of(context).size.width *0.4,
                child: Icon(Icons.card_giftcard,
                    color: PRIMARY_COLOR,
                    size: 60, ),)
            ),
          ),
        ],
      ),
    );
  }

  void _showPayment() {
    showDialog(
     context: context,
     builder: (BuildContext context){
       return AlertDialog(
         content: Container(
           height: MediaQuery.of(context).size.height *0.4,
           child: Column(
             children: [
              Image.network("https://images.unsplash.com/photo-1525088553748-01d6e210e00b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80"),
              SizedBox(height: 10,),
              Text("¡Orden exitosa!",
              style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(fontWeight: FontWeight.bold,
                          )
              ),
              SizedBox(height: 20,),
              Text("Tu orden ha sido registrada, para más información busca en la opción historial de compras en tu perfil.",
              style: Theme.of(context)
                          .textTheme
                          .bodyText1
                          .copyWith(fontWeight: FontWeight.normal,
                          fontSize: 16,
                        
                          )
              ),
            ],
          ),
        ),
         actions: <Widget>[
           new FlatButton(
             onPressed: () {
               Navigator.of(context).pop();
             }, 
           child: Text("ACEPTAR",
            style: TextStyle(
              color: Colors.purple[800],
              fontSize: 16),
              )
           )
         ]);
     }
   );

  }

  
}
