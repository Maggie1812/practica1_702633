import 'package:flutter/material.dart';

// home
const String APP_TITLE = "Coffe shop";
// profile
const String PROFILE_TITLE = "Perfil";
const String PROFILE_LOGOUT = "Cerrar sesion";
const String PROFILE_CART = "Lista de compras";
const String PROFILE_WISHES = "Lista de deseos";
const String PROFILE_HISTORY = "Historial de compras";
const String PROFILE_SETTINGS = "Ajustes";
const String PROFILE_NAME = "Anna Doe";
const String PROFILE_EMAIL = "anna@doe.com";
const String PROFILE_PICTURE =
    "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60";

// app
const Color PRIMARY_COLOR = Color(0xFF214254);
const Color ACCENT_COLOR = Color(0xFF121B22);
const Color BACKGROUND_COLOR = Color(0xFFBCB0A1);
const Color BUTTON_COLOR = Color(0xFF8B8175);
const Color CART_ITEM_COLOR = Color(0xFFFABF7C);
