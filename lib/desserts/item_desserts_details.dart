import 'package:estructura_practica_1/cart/cart.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/utils/constants.dart';

import 'package:flutter/material.dart';
import 'package:estructura_practica_1/models/product_dessert.dart';

class ItemDessertsDetails extends StatefulWidget {
  ProductDessert dessert;
  List<ProductItemCart> productList;
  ItemDessertsDetails({
    Key key,
    @required this.dessert,
    @required this.productList
  }) : super(key: key);

  @override
  _DessertDetailsState createState() => _DessertDetailsState();
  }
  

class _DessertDetailsState extends State<ItemDessertsDetails>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var _selectedIndex = OrderSize.M;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey, 
      appBar: AppBar(
        title: Text(widget.dessert.productTitle),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Cart(
                  productsList: widget.productList)
                  ),
               );
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 48, horizontal: 24),
                height: MediaQuery.of(context).size.height / 3,
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Image.network(
                          widget.dessert.productImage, 
                          height: 160,
                          width: 160
                        )
                      )
                    ),
                   // Container(
                     // color: Colors.orange,
                   // ),
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        icon: Icon(Icons.favorite,
                        color: widget.dessert.liked? Colors.redAccent : PRIMARY_COLOR),
                        onPressed: () {
                          setState(() {
                            widget.dessert.liked = !widget.dessert.liked;
                          });
                        },
                      ),
                    ),

                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 28),
                child: Text(widget.dessert.productTitle),
              ),
              Text(widget.dessert.productDescription),
              SizedBox(
                height: 48,
              ),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Text("TAMAÑOS DISPONIBLES"),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Text("Precio"),
                        Text("\$ " + widget.dessert.productPrice.toString()),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ChoiceChip(
                    label: Text('Chico'),
                    labelStyle: TextStyle(
                      color: _selectedIndex == OrderSize.CH ? Colors.white : Colors.blue,
                    ),
                    selected: _selectedIndex == OrderSize.CH,
                    backgroundColor: Colors.white,
                    selectedColor: Colors.blue,
                    onSelected: (bool selected) {
                      setState(() {
                        if (selected){
                          _selectedIndex = OrderSize.CH;
                          widget.dessert.orderSize = OrderSize.CH;
                          widget.dessert.productPrice = widget.dessert.productPriceCalculator();
                        }                       
                      });
                    },
                  ),
                  ChoiceChip(
                    label: Text('Mediano'),
                    selected: _selectedIndex == OrderSize.M,
                    labelStyle: TextStyle(
                      color: _selectedIndex == OrderSize.M ? Colors.white : Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    selectedColor: Colors.blue,
                    onSelected: (bool selected) {
                      setState(() {
                        if (selected) {
                          _selectedIndex = OrderSize.M;
                          widget.dessert.orderSize = OrderSize.M;
                          widget.dessert.productPrice = widget.dessert.productPriceCalculator();

                        }
                      });
                    },
                  ),
                  ChoiceChip(
                    label: Text('Grande'),
                    selected: _selectedIndex == OrderSize.G,
                    labelStyle: TextStyle(
                      color: _selectedIndex == OrderSize.G ? Colors.white : Colors.blue,
                    ),
                    backgroundColor: Colors.white,
                    selectedColor: Colors.blue,
                    onSelected: (bool selected) {
                      setState(() {
                        if (selected){
                          _selectedIndex = OrderSize.G;
                        widget.dessert.orderSize = OrderSize.G;
                        widget.dessert.productPrice = widget.dessert.productPriceCalculator();
                        }
                      });
                    },
                  ),
                ]
              ),
              Row(
                children: [SizedBox(
                  height: 80,
                )]
              ),
              Row(
                children: [
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {
                        widget.dessert.productAmount += 1;
                        widget.productList.retainWhere((element) => element.productTitle != widget.dessert.productTitle);
                        ProductItemCart _productItemCart = ProductItemCart(
                          productAmount: widget.dessert.productAmount,
                          productTitle: widget.dessert.productTitle,
                          productPrice: widget.dessert.productPrice,
                          productImage: widget.dessert.productImage,
                          productDescription: widget.dessert.productDescription,
                          productSize: widget.dessert.orderSize.toString());
                          widget.productList.add(_productItemCart);
                      },
                      child: Text("AGREGAR AL CARRITO",
                      textAlign: TextAlign.center),
                      color: Colors.grey[300],
                      height: 60,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {},
                      child: Text("COMPRAR AHORA",
                      textAlign: TextAlign.center),
                      color: Colors.grey[300],
                      height: 60,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );

  }
  
}