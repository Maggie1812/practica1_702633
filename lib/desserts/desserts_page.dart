import 'package:estructura_practica_1/desserts/item_desserts.dart';
import 'package:estructura_practica_1/desserts/item_desserts_details.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/models/product_dessert.dart';
import 'package:estructura_practica_1/cart/cart.dart';


class DessertsPage extends StatefulWidget {
  final List<ProductDessert> dessertsList;
  List<ProductItemCart> productList;
  DessertsPage({
    Key key,
    @required this.dessertsList,
    @required this.productList,
  }) : super(key: key);

@override
  _DessertsPageState createState() => _DessertsPageState();
}

class _DessertsPageState extends State<DessertsPage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Postres"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Cart(
                  productsList: widget.productList)
                  ),
               );
            },
          )
        ],
      ),
      body: ListView.builder(
        itemCount: widget.dessertsList.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => ItemDessertsDetails(
                    dessert: widget.dessertsList[index],
                    productList: widget.productList
                  )
                )
              );
            },
            child:  ItemDesserts(
              dessert: widget.dessertsList[index],
            )
          ); 
        },
      ),
    );
  }

}
