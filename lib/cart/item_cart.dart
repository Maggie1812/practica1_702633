import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

class ItemCart extends StatefulWidget {
  final dynamic product;
  List<ProductItemCart> productList;
  final ValueChanged<double> onAmountUpdated;
  ItemCart({
    Key key,
    @required this.onAmountUpdated,
    @required this.product,             
    @required this.productList,
  }) : super(key: key);

  @override
  _ItemCartState createState() => _ItemCartState();
}

class _ItemCartState extends State<ItemCart> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: CART_ITEM_COLOR,
      margin: EdgeInsets.all(24),
      child: Stack(
        children: <Widget>[
          Row(
            children: [
              SizedBox(width: 10,),
              Align(
                alignment: Alignment.centerLeft,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(5.0),
                    bottomRight: Radius.circular(5.0),
                  ),
                  child: Image.network(
                    "${widget.product.productImage}",
                    fit: BoxFit.contain,
                    height: 150,
                    width: MediaQuery.of(context).size.width * 0.25,
                  ),
                ),
              ),
              Column(
                children: <Widget>[
                SizedBox(
                height: 12,
                ),
                Text("${widget.product.productTitle}",
                  style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(fontWeight: FontWeight.bold,
                            fontSize: 20),
                        textAlign: TextAlign.center,),
                SizedBox(
                  height: 12,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.55,
                  child: Text("${widget.product.productDescription}",
                    style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(fontWeight: FontWeight.normal,
                            fontSize: 16),
                        textAlign: TextAlign.center,)
                ),
                SizedBox(
                  height: 12,
                ),
                Text("${widget.product.productSize}"),
                SizedBox(
                  height: 12,
                ),
                
                SizedBox(
                  height: 12,
                ),

              Row( 
                  children: <Widget>[
                    IconButton(icon: Icon(Icons.add_circle_outline), onPressed: _addProd),
                    Text("${widget.product.productAmount}"),
                    IconButton(icon: Icon(Icons.remove_circle), onPressed: _remProd),
                    SizedBox(width: 12),
                    Text("\$${widget.product.productPrice}",
                        style: TextStyle(fontSize: 24)
                    ),
                    SizedBox(width: 12),
                    IconButton(icon: Icon(Icons.remove_shopping_cart ), onPressed: _delProd)
                  ]
                ),
            ],
          ), 
        ],    
      )
        ]
    )
    ,);
  }

  void _addProd() {
    setState(() {
      ++widget.product.productAmount;
    });
    widget.onAmountUpdated(widget.product.productPrice);
  }

  void _remProd() {
    setState(() {
      --widget.product.productAmount;
    });
    widget.onAmountUpdated(-1 * widget.product.productPrice);
  }

  
  void _delProd() {
    setState(() {
      widget.productList.remove(widget.product);
    });
  }
}
