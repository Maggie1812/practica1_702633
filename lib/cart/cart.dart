import 'package:flutter/material.dart';
import 'package:estructura_practica_1/cart/item_cart.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/pay/pay.dart';



class Cart extends StatefulWidget {
  List<ProductItemCart> productsList;
  Cart({
    Key key,
    @required this.productsList,
  }) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  double _total = 0;
  @override
  void initState() {
    super.initState();
    for (var item in widget.productsList) {
      _total += (item.productPrice * item.productAmount);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Mi carrito"),
      ),
      body: Stack(
        children: <Widget>[
          Column(children: [
            Container(
            height: MediaQuery.of(context).size.height * 0.7,
            child: ListView.builder(
              itemCount: widget.productsList.length,
              itemBuilder: (BuildContext context, int index) {
                return ItemCart(
                  onAmountUpdated: _priceUpdate,
                  product: widget.productsList[index],
                  productList: widget.productsList
                );
              },
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, top: 10),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Total:",
                      style: TextStyle(fontSize: 20)
                    ),
                  )
                ),
                Container(
                  margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, top: 10),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                     " \$$_total MXN",
                      style: TextStyle(fontSize: 30)
                    ),
                  )
                ),
                SizedBox(height: 10,),
                Container(
                child: FlatButton(
                  color: Color(0xFF8B8175),
                  shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
                  child: Text("PAGAR"),
                  onPressed: _gotoPay,
                ),
                height: 50,
                width: MediaQuery.of(context).size.width * 0.90,
              ),
              ],
            )),
          ],)
          
        ],
      ),
    );
  }

  void _priceUpdate(double newItemPrice) {
    setState(() {
      _total += newItemPrice;
    });
  }

  void _gotoPay(){
       Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => Pay()),
    );
  }
}
