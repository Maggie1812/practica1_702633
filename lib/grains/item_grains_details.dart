import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/models/product_grains.dart';
import 'package:estructura_practica_1/cart/cart.dart';
import 'package:estructura_practica_1/utils/constants.dart';

 
class ItemGrainsDetails extends StatefulWidget {
  ProductGrains grain;
  List<ProductItemCart> productList;
  ItemGrainsDetails({
    Key key,
    @required this.grain,
    @required this.productList,
  }) : super(key: key);

  @override
  _GrainsDetailState createState() => _GrainsDetailState();
  }
  

class _GrainsDetailState extends State<ItemGrainsDetails>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var _selectedIndex = ProductWeight.KILO;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey, 
      appBar: AppBar(
        title: Text(widget.grain.productTitle),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Cart(
                  productsList: widget.productList)
                  ),
               );
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 48, horizontal: 24),
                height: MediaQuery.of(context).size.height / 3,
                child: Stack(
                  children: [
                    Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child: Image.network(
                          widget.grain.productImage, 
                          height: 160,
                          width: 160
                        )
                      )
                    ),
                   // Container(
                     // color: Colors.orange,
                   // ),
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        icon: Icon(Icons.favorite,
                        color: widget.grain.liked? Colors.redAccent : PRIMARY_COLOR),
                        onPressed: () {
                          setState(() {
                            widget.grain.liked = !widget.grain.liked;
                          });
                        },
                      ),
                    ),

                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 28),
                child: Text(widget.grain.productTitle),
              ),
              Text(widget.grain.productDescription),
              SizedBox(
                height: 48,
              ),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Text("TAMAÑOS DISPONIBLES"),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Text("Precio"),
                        Text("\$ " + widget.grain.productPrice.toString()),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ChoiceChip(
                    label: Text('CUARTO'),
                    labelStyle: TextStyle(
                      color: _selectedIndex == ProductWeight.CUARTO ? Colors.white : Colors.blue,
                    ),
                    selected: _selectedIndex == ProductWeight.CUARTO,
                    backgroundColor: Colors.white,
                    selectedColor: Colors.blue,
                    onSelected: (bool selected) {
                      setState(() {
                        if (selected){
                          _selectedIndex = ProductWeight.CUARTO;
                          widget.grain.productWeight = ProductWeight.CUARTO;
                          widget.grain.productPrice = widget.grain.productPriceCalculator();
                        }                       
                      });
                    },
                  ),
                  ChoiceChip(
                    label: Text('KILO'),
                    selected: _selectedIndex == ProductWeight.KILO,
                    labelStyle: TextStyle(
                      color: _selectedIndex == ProductWeight.KILO ? Colors.white : Color(0xFF121B22),
                    ),
                    backgroundColor: Colors.white,
                    selectedColor: Colors.blue,
                    onSelected: (bool selected) {
                      setState(() {
                        if (selected) {
                          _selectedIndex = ProductWeight.KILO;
                          widget.grain.productWeight = ProductWeight.KILO;
                          widget.grain.productPrice = widget.grain.productPriceCalculator();

                        }
                      });
                    },
                  ),
                  SizedBox(width: 15)
                ]
              ),
              Row(
                children: [SizedBox(
                  height: 80,
                )]
              ),
              Row(
                children: [
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {
                        widget.grain.productAmount += 1;
                        widget.productList.retainWhere((element) => element.productTitle != widget.grain.productTitle);
                        ProductItemCart _productItemCart = ProductItemCart(
                          productAmount: widget.grain.productAmount,
                          productTitle: widget.grain.productTitle,
                          productPrice: widget.grain.productPrice,
                          productImage: widget.grain.productImage,
                          productDescription: widget.grain.productDescription,
                          productSize: widget.grain.productWeight.toString());
                          widget.productList.add(_productItemCart);
                      },
                      child: Text("AGREGAR AL CARRITO",
                      textAlign: TextAlign.center),
                      color: Colors.grey[300],
                      height: 60,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {},
                      child: Text("COMPRAR AHORA",
                      textAlign: TextAlign.center),
                      color: Colors.grey[300],
                      height: 60,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );

  }
  
}