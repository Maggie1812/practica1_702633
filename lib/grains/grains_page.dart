import 'package:estructura_practica_1/models/product_grains.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/cart/cart.dart';

import 'package:flutter/material.dart';
import 'package:estructura_practica_1/grains/item_grains.dart';
import 'package:estructura_practica_1/grains/item_grains_details.dart';

class GrainsPage extends StatefulWidget {
  final List<ProductGrains> grainList;
  List<ProductItemCart> productList;
  GrainsPage({
    Key key,
    @required this.grainList,
    @required this.productList,
  }) : super(key: key);

 

  @override
  _GrainsPageState createState() => _GrainsPageState();
  

}
  class _GrainsPageState extends State<GrainsPage> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Granos"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Cart(
                  productsList: widget.productList)
                  ),
               );
            },
          )
        ],
      ),
      body: ListView.builder(
        itemCount: widget.grainList.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => ItemGrainsDetails(
                    grain: widget.grainList[index],
                    productList: widget.productList
                  )
                )
              );
            },
            child:  ItemGrains(
              grain: widget.grainList[index]
            )
          );
        },
      ),
    );
  }
  }