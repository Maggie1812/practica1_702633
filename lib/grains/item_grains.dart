import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/models/product_grains.dart';

class ItemGrains extends StatelessWidget {
  final ProductGrains grain;
  ItemGrains({
    Key key,
    @required this.grain
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
  return Container(
      height: 220,
      child: Stack(
        children: [
          Container(
            height: 200,
            margin: EdgeInsets.only(top: 20, bottom: 20, left: 24, right: 24),
            decoration: BoxDecoration(
              color: BACKGROUND_COLOR,
              borderRadius: BorderRadius.all(
                Radius.circular(2),
              ),
            ),
            child: Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 6),
                    width: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text(
                        "${grain.productTitle}",
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(fontWeight: FontWeight.bold,
                            fontSize: 20),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 6),
                    width: 160,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        "${grain.productDescription}",
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(fontWeight: FontWeight.normal),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  Align(alignment: Alignment.bottomLeft,
                  child: Container(
                    margin: EdgeInsets.only(left: 6),
                    width: 160,
                    height: 50,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(30),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        "\$${grain.productPrice}",
                        style: Theme.of(context)
                            .textTheme
                            .headline5
                            .copyWith(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ),
                  )
                ],
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(5.0),
                      bottomRight: Radius.circular(5.0),
                    ),
                    child: Image.network(
                      "${grain.productImage}",
                      fit: BoxFit.contain,
                      height: 180,
                      width: 130,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}


