import 'package:estructura_practica_1/desserts/desserts_page.dart';
import 'package:estructura_practica_1/drinks/hot_drinks_page.dart';
import 'package:estructura_practica_1/grains/grains_page.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/home/item_home.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:estructura_practica_1/models/product_repository.dart';
import 'package:estructura_practica_1/cart/cart.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';



class Home extends StatefulWidget {
  final String title;
  List<ProductItemCart> _productsList = [];

  Home({Key key, this.title}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
         
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => Cart(
                  productsList: widget._productsList)),
               );
            },
          )
        ],
      ),
      drawer:  Drawer(
        child: ListView(
          padding: EdgeInsets.all(24.0),
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 20,),
                Container(
                  height: 150,
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(
                      PROFILE_PICTURE,
                    ),
                    minRadius: 40,
                    maxRadius: 80,
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(
                  PROFILE_NAME,
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      .copyWith(color: Colors.black),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(PROFILE_EMAIL),
                SizedBox(
                  height: 16,
                ),
                ListTile(
                  title: Text(PROFILE_CART),
                  leading: Icon(Icons.shopping_cart),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(PROFILE_WISHES),
                  leading: Icon(Icons.thumb_up),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(PROFILE_HISTORY),
                  leading: Icon(Icons.store),
                  onTap: () {},
                ),
                ListTile(
                  title: Text(PROFILE_SETTINGS),
                  leading: Icon(Icons.settings),
                  onTap: () {},
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: RaisedButton(
                      child: Text(PROFILE_LOGOUT),
                      onPressed: () {},
                    ),
                  ),
                ],
              ),
            ),
          ],
        
      ),
          ),
      body: ListView(
        children: <Widget>[
          GestureDetector(
            onTap: _openHotDrinksPage,
            child: ItemHome(
              title: "Bebidas calientes",
              image: "https://i.imgur.com/XJ0y9qs.png",
            ),
          ),
          GestureDetector(
            onTap: _openGrainsPage,
            child: ItemHome(
              title: "Granos",
              image: "https://i.imgur.com/5MZocC1.png",
            ),
          ),
          GestureDetector(
            onTap: _openDessertPage,
            child: ItemHome(
              title: "Postres",
              image: "https://i.imgur.com/fI7Tezv.png",
            ),
          ),
          
          GestureDetector(     
            onTap: _showComingSoon,     
            child: ItemHome(
              title: "Tazas",
              image: "https://i.imgur.com/fMjtSpy.png", 
            ),
          )
        ],
      ),
    );
  }

  void _openHotDrinksPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => HotDrinksPage(
        drinksList: ProductRepository.loadProducts(ProductType.BEBIDAS),
        productList: widget._productsList)),
    );
  }

  void _openGrainsPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => GrainsPage(
        grainList: ProductRepository.loadProducts(ProductType.GRANO),
        productList: widget._productsList)),
    );
  }

  void _openDessertPage() {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => DessertsPage(
        dessertsList: ProductRepository.loadProducts(ProductType.POSTRES),
        productList: widget._productsList)),
    );
  }

  void _showComingSoon() {
    var _snackBar = SnackBar(
     content: Text('Coming Soon'),
     duration: Duration(milliseconds: 500)
     );
     _scaffoldKey.currentState.showSnackBar(_snackBar);
  }
}
